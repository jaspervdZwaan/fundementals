﻿using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.User
{
    public class AuthenticateUserResource
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
