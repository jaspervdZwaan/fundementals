﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.User
{
    public class UpdateUserResource
    {
        [StringLength(15)]
        public string Username { get; set; }

        [StringLength(320)]
        public string Email { get; set; }

        [StringLength(280)]
        public string Description { get; set; }

        public string Location { get; set; }

        public IFormFile Image { get; set; }
    }
}
