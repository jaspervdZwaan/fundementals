﻿namespace Resources.Entities.User
{
    public class AuthenticationResultResource
    {
        public string Token { get; set; }
    }
}
