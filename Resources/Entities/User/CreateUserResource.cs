﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.User
{
    public class CreateUserResource
    {
        [Required]
        [StringLength(15)]
        public string Username { get; set; }

        [Required]
        [StringLength(320)]
        public string Email { get; set; }

        [Required]
        [StringLength(72)]
        public string Password { get; set; }

        [StringLength(280)]
        public string Description { get; set; }

        public string Location { get; set; }

        public IFormFile Image { get; set; }
    }
}
