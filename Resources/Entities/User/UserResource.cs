﻿using System;

namespace Resources.Entities.User
{
    public class UserResource
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }

        public string Image { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
