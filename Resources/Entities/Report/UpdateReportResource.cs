﻿using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.Report
{
    public class UpdateReportResource
    {
        [StringLength(280)]
        public string Description { get; set; }

        public string Category { get; set; }
    }
}
