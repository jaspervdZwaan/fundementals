﻿using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.Report
{
    public class CreateReportResource
    {
        [Required]
        public int MessageId { get; set; }

        [StringLength(280)]
        public string Description { get; set; }

        public string Category { get; set; }
    }
}
