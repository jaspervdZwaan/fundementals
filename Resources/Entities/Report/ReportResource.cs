﻿using Resources.Entities.Message;
using Resources.Entities.User;
using System;

namespace Resources.Entities.Report
{
    public class ReportResource
    {
        public int Id { get; set; }

        public UserResource User { get; set; }

        public MessageResource Message { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
