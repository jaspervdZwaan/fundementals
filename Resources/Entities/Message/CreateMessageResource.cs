﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.Message
{
    public class CreateMessageResource
    {
        [StringLength(280)]
        public string Text { get; set; }

        [FromForm(Name = "Image")]
        public IFormFile Image { get; set; }

        public Location Location { get; set; }
    }
}
