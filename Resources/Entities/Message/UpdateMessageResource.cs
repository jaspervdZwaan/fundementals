﻿using Microsoft.AspNetCore.Http;
using Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace Resources.Entities.Message
{
    public class UpdateMessageResource
    {
        [StringLength(280)]
        public string Text { get; set; }

        public IFormFile Image { get; set; }

        public Location Location { get; set; }
    }
}
