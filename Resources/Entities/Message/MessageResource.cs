﻿using Models.Entities;
using Resources.Entities.User;
using System;

namespace Resources.Entities.Message
{
    public class MessageResource
    {
        public int Id { get; set; }

        public UserResource User { get; set; }

        public string Text { get; set; }

        public string Image { get; set; }

        public Location Location { get; set; }

        public int LikeCount { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
