﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Logging;
using Models.Entities;
using Models.Relations;

namespace Data.Contexts
{
    public class FundementalsDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<Friendship> Friendships { get; set; }

        public FundementalsDbContext(DbContextOptions<FundementalsDbContext> options) : base(options)
        {
            
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseLoggerFactory(MyLoggerFactory).UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\jaspe\Source\Repos\fundmentals\Data\Database.mdf;Integrated Security=True");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _ = modelBuilder.Entity<Message>().OwnsOne(message => message.Location);
        }

        public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddConsole(); });
    }

    public class DatabaseContextFactory : IDesignTimeDbContextFactory<FundementalsDbContext>
    {
        public FundementalsDbContext CreateDbContext(string[] args)
        {
            return new FundementalsDbContext(new DbContextOptionsBuilder<FundementalsDbContext>().Options);
        }
    }
}
