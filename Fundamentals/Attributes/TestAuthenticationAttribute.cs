﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Fundamentals.Attributes
{
    public class TestAuthenticationAttribute : TypeFilterAttribute
    {
        public TestAuthenticationAttribute() : base(typeof(TestAuthenticationImplemenatation)) { }

        private class TestAuthenticationImplemenatation : IAsyncActionFilter
        {
            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                context.HttpContext.User = new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, "1")
                }));

                await next();
            }
        }
    }
}
