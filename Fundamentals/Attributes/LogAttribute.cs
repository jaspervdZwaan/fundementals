﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Fundamentals.Attributes
{
    public class LogAttribute : ActionFilterAttribute
    {
        public ILogger Logger;

        public LogAttribute(ILogger logger)
        {
            Logger = logger;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Logger.LogInformation(context.HttpContext.Request.ToString());
        }
    }
}
