﻿using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace Fundamentals.Attributes
{
    public class ValidateMessageModificationAttribute : TypeFilterAttribute
    {
        public ValidateMessageModificationAttribute() : base(typeof(ValidateMessageModificationImplementation)) { }

        private class ValidateMessageModificationImplementation : IAsyncActionFilter
        {
            private readonly IMessageService MessageService;

            public ValidateMessageModificationImplementation(IMessageService messageService)
            {
                MessageService = messageService;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (context.ActionArguments.ContainsKey("id"))
                {
                    int? id = context.ActionArguments["id"] as int?;

                    if (id.HasValue)
                    {
                        if ((await MessageService.GetByIdAsync(id.Value)).UserId != Convert.ToInt32(context.HttpContext.User.Identity.Name))
                        {
                            context.Result = new UnauthorizedObjectResult(id.Value);
                            return;
                        }
                    }
                }

                await next();
            }
        }
    }
}
