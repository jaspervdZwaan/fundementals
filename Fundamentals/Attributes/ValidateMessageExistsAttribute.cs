﻿using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace Fundamentals.Attributes
{
    public class ValidateMessageExistsAttribute : TypeFilterAttribute
    {
        public ValidateMessageExistsAttribute() : base(typeof(ValidateMessageExistenceImplementation)) { }

        private class ValidateMessageExistenceImplementation : IAsyncActionFilter
        {
            private readonly IMessageService MessageService;

            public ValidateMessageExistenceImplementation(IMessageService messageService)
            {
                MessageService = messageService;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (context.ActionArguments.ContainsKey("id"))
                {
                    int? id = context.ActionArguments["id"] as int?;

                    if (id.HasValue)
                    {
                        if (await MessageService.GetByIdAsync(id.Value) == null)
                        {
                            context.Result = new NotFoundObjectResult(id.Value);
                            return;
                        }
                    }
                }

                await next();
            }
        }
    }
}
