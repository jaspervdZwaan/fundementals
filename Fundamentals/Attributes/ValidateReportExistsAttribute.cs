﻿using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace Fundamentals.Attributes
{
    public class ValidateReportExistsAttribute : TypeFilterAttribute
    {
        public ValidateReportExistsAttribute() : base(typeof(ValidateReportExistsImplementation)) { }

        private class ValidateReportExistsImplementation : IAsyncActionFilter
        {
            private readonly IReportService ReportService;

            public ValidateReportExistsImplementation(IReportService reportService)
            {
                ReportService = reportService;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (context.ActionArguments.ContainsKey("id"))
                {
                    int? id = context.ActionArguments["id"] as int?;

                    if (id.HasValue)
                    {
                        if (await ReportService.GetByIdAsync(id.Value) == null)
                        {
                            context.Result = new NotFoundObjectResult(id.Value);
                            return;
                        }
                    }
                }

                await next();
            }
        }
    }
}
