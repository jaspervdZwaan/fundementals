﻿using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace Fundamentals.Attributes
{
    public class ValidateReportModificationAttribute : TypeFilterAttribute
    {
        public ValidateReportModificationAttribute() : base(typeof(ValidateReportModificationImplementation)) { }

        private class ValidateReportModificationImplementation : IAsyncActionFilter
        {
            private readonly IReportService ReportService;

            public ValidateReportModificationImplementation(IReportService reportService)
            {
                ReportService = reportService;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (context.ActionArguments.ContainsKey("id"))
                {
                    int? id = context.ActionArguments["id"] as int?;

                    if (id.HasValue)
                    {
                        if ((await ReportService.GetByIdAsync(id.Value)).UserId != Convert.ToInt32(context.HttpContext.User.Identity.Name))
                        {
                            context.Result = new UnauthorizedObjectResult(id.Value);
                            return;
                        }
                    }
                }

                await next();
            }
        }
    }
}
