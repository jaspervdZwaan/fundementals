﻿using AutoMapper;
using Fundamentals.Attributes;
using Fundamentals.Extensions;
using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Resources.Entities.Report;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fundamentals.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    [ValidateModel]
    [ValidateReportExists]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class ReportsController : ControllerBase
    {
        private readonly IReportService ReportService;

        private readonly IMapper Mapper;

        public ReportsController(IReportService reportService, IMapper mapper)
        {
            ReportService = reportService;
            Mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<ReportResource>> GetReportsAsync()
        {
            return Mapper.MapList<ReportResource>(await ReportService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ReportResource> GetReportAsync(int id)
        {
            return Mapper.Map<ReportResource>(await ReportService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateReportAsync([FromBody]CreateReportResource resource)
        {
            var result = await ReportService.CreateAsync((Report)Mapper.Map<Report>(resource).SetPropertyInline("UserId", Convert.ToInt32(User.Identity.Name)));
            return result.CompletedSuccesfully ? CreatedAtAction(nameof(GetReportAsync), new { id = result.Entity.Id }, Mapper.Map<ReportResource>(result.Entity)) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpDelete]
        [ValidateReportModification]
        public async Task<IActionResult> RemoveReportAsync(int id)
        {
            var result = await ReportService.RemoveAsync(id);
            return result.CompletedSuccesfully ? Ok(id) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpPut("{id}")]
        [ValidateReportModification]
        public async Task<IActionResult> UpdateReportAsync([FromBody]UpdateReportResource resource, int id)
        {
            var result = await ReportService.UpdateAsync(Mapper.Map<Report>(resource), id);
            return result.CompletedSuccesfully ? Ok(Mapper.Map<ReportResource>(result.Entity)) : (IActionResult)Conflict(result.ErrorMessage);
        }
    }
}
