﻿using AutoMapper;
using Fundamentals.Attributes;
using Fundamentals.Extensions;
using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models.Entities;
using Resources.Entities.User;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fundamentals.Controllers
{
    [ApiController]
    [Route("api/v{v:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [ValidateModel]
    public class UsersController : ControllerBase
    {
        private readonly IUserService UserService;

        private readonly IMapper Mapper;

        public UsersController(IUserService userService, IMapper mapper)
        {
            UserService = userService;
            Mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<UserResource>> GetUsersAsync()
        {
            return Mapper.MapList<UserResource>(await UserService.GetAllAsync());
        }

        [HttpGet("{id}")]
        [ResponseCache(Duration = 30)]
        public async Task<UserResource> GetUserAsync(int id)
        {
            return Mapper.Map<UserResource>(await UserService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserAsync([FromForm] CreateUserResource resource)
        {
            var result = await UserService.CreateAsync(Mapper.Map<User>(resource));
            return result.CompletedSuccesfully ? CreatedAtAction(nameof(GetUserAsync), new { id = result.Entity.Id }, Mapper.Map<UserResource>(result.Entity)) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> RemoveUserAsync()
        {
            var result = await UserService.RemoveAsync(Convert.ToInt32(User.Identity.Name));
            return result.CompletedSuccesfully ? Ok() : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> UpdateUserAsync([FromBody] UpdateUserResource resource)
        {
            var result = await UserService.UpdateAsync(Mapper.Map<User>(resource), Convert.ToInt32(User.Identity.Name));
            return result.CompletedSuccesfully ? Ok(Mapper.Map<UserResource>(result.Entity)) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateUserAsync([FromBody] AuthenticateUserResource user)
        {
            string token = await UserService.AuthenticateAsync(Mapper.Map<User>(user));
            return token == null ? Unauthorized() : (IActionResult)Ok(new AuthenticationResultResource() { Token = token });
        }

        [HttpGet("get")]
        [ResponseCache(Duration = 30)]
        public async Task<UserResource> GetUserByUsernameAsync([FromQuery] string username)
        {
            return Mapper.Map<UserResource>(await UserService.GetByUsername(username));
        }

        [HttpGet("search")]
        public async Task<IEnumerable<UserResource>> SearchAsync([FromQuery] string query)
        {
            return Mapper.MapList<UserResource>(await UserService.Search(query));
        }
    }
}
