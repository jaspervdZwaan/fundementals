﻿using Fundamentals.Attributes;
using Interfaces.Services.Relations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Relations;
using Resources.Relations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fundamentals.Controllers
{
    [Route("api/v{v:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [ValidateModel]
    [Authorize]
    public class FriendshipsController : ControllerBase
    {
        private readonly IFriendshipService FriendshipService;

        public FriendshipsController(IFriendshipService friendshipService)
        {
            FriendshipService = friendshipService;
        }

        [HttpGet]
        public async Task<IEnumerable<Friendship>> GetAllAsync()
        {
            return await FriendshipService.GetAllAsync();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateFriendshipResource friendship)
        {
            var result = await FriendshipService.CreateAsync(new Friendship() { UserId = int.Parse(User.Identity.Name), FriendId = friendship.FriendId });
            return result.CompletedSuccesfully ? Ok(result.Entity) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveAsync([FromBody] CreateFriendshipResource friendship)
        {
            var result = await FriendshipService.RemoveAsync(new Friendship() { UserId = int.Parse(User.Identity.Name), FriendId = friendship.FriendId });
            return result.CompletedSuccesfully ? Ok(result.Entity) : (IActionResult)Conflict(result.ErrorMessage);
        }
    }
}
