﻿using AutoMapper;
using Fundamentals.Attributes;
using Fundamentals.Extensions;
using Interfaces.Services.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Entities;
using Resources.Entities.Message;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Fundamentals.Controllers
{
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    [ValidateModel]
    [ValidateMessageExists]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class MessagesController : ControllerBase
    {
        private readonly IMessageService MessageService;

        private readonly IMapper Mapper;

        public MessagesController(IMessageService messageService, IMapper mapper)
        {
            MessageService = messageService;
            Mapper = mapper;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IEnumerable<MessageResource>> GetMessagesAsync()
        {
            return Mapper.MapList<MessageResource>(await MessageService.GetAllAsync());
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<MessageResource> GetMessageAsync(int id)
        {
            return Mapper.Map<MessageResource>(await MessageService.GetByIdAsync(id));
        }

        [HttpPost]
        public async Task<IActionResult> CreateMessageAsync([FromForm] CreateMessageResource resource)
        {
            var result = await MessageService.CreateAsync((Message)Mapper.Map<Message>(resource).SetPropertyInline("UserId", int.Parse(User.Identity.Name)));
            return result.CompletedSuccesfully ? CreatedAtAction(nameof(GetMessageAsync), new { id = result.Entity.Id }, Mapper.Map<MessageResource>(result.Entity)) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpDelete("{id}")]
        [ValidateMessageModification]
        public async Task<IActionResult> RemoveMessageAsync(int id)
        {
            var result = await MessageService.RemoveAsync(id);
            return result.CompletedSuccesfully ? Ok(id) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpPut("{id}")]
        [ValidateMessageModification]
        public async Task<IActionResult> UpdateMessageAsync([FromBody] UpdateMessageResource resource, int id)
        {
            var result = await MessageService.UpdateAsync(Mapper.Map<Message>(resource), id);
            return result.CompletedSuccesfully ? Ok(result.Entity) : (IActionResult)Conflict(result.ErrorMessage);
        }

        [HttpGet("search")]
        [AllowAnonymous]
        public async Task<IEnumerable<MessageResource>> SearchAsync([FromQuery]string query)
        {
            return Mapper.MapList<MessageResource>(await MessageService.Search(query));
        }

        [HttpGet("user_feed")]
        [AllowAnonymous]
        public async Task<IEnumerable<MessageResource>> GetUserFeedAsyc([FromQuery]string username)
        {
            return Mapper.MapList<MessageResource>(await MessageService.GetUserFeed(username));
        }

        [HttpGet("home_feed")]
        public async Task<IEnumerable<MessageResource>> GetHomeFeedAsync()
        {
            return Mapper.MapList<MessageResource>(await MessageService.GetHomeFeed(Convert.ToInt32(User.Identity.Name)));
        }
    }
}
