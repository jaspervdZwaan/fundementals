using AutoMapper;
using Data.Contexts;
using FestiFind.Logic.Services;
using Fundamentals.Attributes;
using Fundamentals.Mapping;
using Interfaces.Services.Authentication;
using Interfaces.Services.Entities;
using Interfaces.Services.Relations;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Services;
using Services.Authentication;
using Services.Entities;
using System.Text;

namespace Fundamentals
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var key = "Kugelblitzes are theoretical.";

            services.AddControllersWithViews();

            services.AddCors(configuration => configuration.AddPolicy("Secure", configuration =>
            {
                configuration.WithOrigins("https://localhost:44396/");
            }));

            services.AddApiVersioning(configuration =>
            {
                configuration.ReportApiVersions = true;
                configuration.AssumeDefaultVersionWhenUnspecified = true;
                configuration.DefaultApiVersion = new ApiVersion(1, 0);
            });

            services.AddDbContextPool<FundementalsDbContext>(configuration =>
            {
                configuration.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddAuthentication(configuration =>
            {
                configuration.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                configuration.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(configuration =>
            {
                configuration.RequireHttpsMetadata = false;
                configuration.SaveToken = true;
                configuration.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //services.AddAuthentication(configuration =>
            //{
            //    configuration.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    configuration.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(configuration =>
            //{
            //    configuration.RequireHttpsMetadata = true;
            //    configuration.SaveToken = true;
            //    configuration.TokenValidationParameters = new TokenValidationParameters()
            //    {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
            //        ValidateIssuer = true,
            //        ValidateAudience = true
            //    };
            //});

            services.AddSingleton<IAuthenticationService>(new AuthenticationService(key));

            services.AddAutoMapper(typeof(Startup));

            IMapper mapper = new MapperConfiguration(configuration =>
            {
                configuration.AddProfile(new EntityToResourceProfile());
                configuration.AddProfile(new ResourceToEntityProfile(new BlobService()));
            }).CreateMapper();

            services.AddSingleton(mapper);

            services.AddResponseCaching();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IReportService, ReportService>();
            services.AddScoped<IFriendshipService, FriendshipService>();

            var provider = services.BuildServiceProvider();

            services.AddMvc(configuration =>
            {
                configuration.Filters.Add(new LogAttribute(provider.GetService<ILogger<Startup>>()));
            });

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseApiVersioning();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<RequestResponseLoggingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
