﻿using AutoMapper;
using Models.Entities;
using Resources.Entities.Message;
using Resources.Entities.User;

namespace Fundamentals.Mapping
{
    public class EntityToResourceProfile : Profile
    {
        public EntityToResourceProfile()
        {
            CreateMap<User, UserResource>();
            CreateMap<Message, MessageResource>();
        }
    }
}
