﻿using AutoMapper;
using Interfaces.Services.Blob;
using Microsoft.AspNetCore.Http;
using Models.Entities;
using Models.Relations;
using Resources.Entities.Message;
using Resources.Entities.User;
using Resources.Relations;
using System.Threading.Tasks;

namespace Fundamentals.Mapping
{
    public class ResourceToEntityProfile : Profile
    {
        private readonly IBlobService BlobService;

        public ResourceToEntityProfile(IBlobService blobService)
        {
            BlobService = blobService;

            CreateMap<CreateUserResource, User>();
            CreateMap<UpdateUserResource, User>();
            CreateMap<AuthenticateUserResource, User>();
            CreateMap<CreateMessageResource, Message>();
            CreateMap<UpdateMessageResource, Message>();
            CreateMap<CreateFriendshipResource, Friendship>();

            CreateMap<IFormFile, string>().ConstructUsing( file =>  GetBlobUrl(file).Result);
        }

        public async Task<string> GetBlobUrl(IFormFile file)
        {
            return await BlobService.UploadAsync(file);
        }
    }
}
    