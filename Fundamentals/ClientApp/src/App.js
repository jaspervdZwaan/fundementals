import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import { Route } from 'react-router';
import { Account } from './components/Account';
import { Explore } from './components/Explore';
import { Feed } from './components/Feed';
import { Home } from './components/Home';
import { Layout } from './components/Layout';
import { NewMessage } from './components/NewMessage';
import { SignIn } from './components/SignIn';
import { SignUp } from './components/SignUp';
import './custom.css';



export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/sign-in' component={SignIn} />
                <Route path='/sign-up' component={SignUp} />
                <Route path='/user/:username' component={Account} />
                <Route path='/explore' component={Explore} />
                <Route path='/new' component={NewMessage} />
                <Route path='/feed' component={Feed} />
            </Layout>
        );
    }
}
