﻿import React, { Component, Fragment } from 'react';
import axios from 'axios';

export class SignUp extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
    }

    render() {
        return (
            <Fragment >
                <div>
                    <form onSubmit={this.handleSubmit} id='Form'>
                        <div className="form-group">
                            <label htmlFor="username">Username</label>
                            <input type='text' className="form-control" ref='username' id="username" name="Username" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email" >Email</label>
                            <input type='email' className="form-control" ref='email' name="Email" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="password" >Password</label>
                            <input type='password' className="form-control" ref='password' name="Password" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="description" >Description</label>
                            <input type='text' className="form-control" ref='description' name="Description" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="location" >Location</label>
                            <input type='text' className="form-control" ref='location' name="Location" />
                        </div>
                        <div className="custom-file">
                            <input type='file' id='file-selector' className="custom-file-input" name="Image" ref="Image" />
                            <label className="custom-file-label" htmlFor="file-selector">Select a profile picture.</label>
                        </div>
                        <button type='submit' className="btn btn-primary" style={{ marginTop: 12 }}>Sign Up</button>
                    </form>
                </div>
            </ Fragment>
        );
    }

    async handleSubmit(event) {
        event.preventDefault();

        axios.post('api/v1.0/users', new FormData(document.getElementById("Form")), {
            headers: {
                'Accept': 'application/json'
            }
        }).then(response => this.handleRedirect(response));
    }

    handleRedirect(response) {
        if (response.status === 201) {
            window.location.href = '/sign-in';
        }
    }
}           