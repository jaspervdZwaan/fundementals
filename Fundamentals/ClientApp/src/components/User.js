﻿import React, { Component } from 'react';

export class User extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var user = this.props.user;

        return (
            <div className='card'>
                <div className='card-body'>
                    <h3 className='card-title'>{user.username}</h3>
                    <p className='card-text'>{user.description}</p>
                </div>
                <img className='card-img-bottom' src={user.image}></img>
                <a class="btn btn-primary" onClick={this.followUser}>Go somewhere</a>
            </div>
        );
    }
}