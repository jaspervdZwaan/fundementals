﻿import React, { Component } from 'react';
import { Message } from './Message';

export class Feed extends Component {
    constructor(props) {
        super(props);
        this.populateFeed = this.populateFeed.bind(this);

        this.state = {
            messages: []
        }
    }

    componentDidMount() {
        this.populateFeed();
    }

    static renderFeed(feed) {
        return (
            <div>
                {
                    feed.map(message => {
                        return (
                            <Message message={message} key={message.id} />
                        );
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div>{Feed.renderFeed(this.state.messages)}</div>
        );
    }

    async populateFeed() {
        const response = await fetch('api/v1.0/messages/home_feed', {
            method: 'Get',
            headers: {
                'Authorization': sessionStorage.getItem('key')
            }
        });
        const data = await response.json();
        console.log(data);
        this.setState({ messages: data });
    }
}