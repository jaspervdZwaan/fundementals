﻿import React, { Component, Fragment } from 'react';
import { UserFeed } from './UserFeed';
import { AccountHeader } from './AccountHeader';

import './css/account.css';

export class Account extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var username = this.props.match.params.username;

        return (
            <Fragment>
                <AccountHeader username={username} />
                <UserFeed username={username} />
            </Fragment>
        );
    }
}
