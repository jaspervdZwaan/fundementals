﻿import React, { Component } from 'react';

export class SignIn extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} id="signin-form">
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type='email' className="form-control" ref='email' id="email" name="Email" />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type='password' className="form-control" ref='password' id="password" name="Password" />
                </div>
                <button type='submit' className="btn btn-primary" id="button">Sign In</button>
            </form>
        );
    }

    async handleSubmit(event) {
        event.preventDefault();

        const response = await fetch('api/v1.0/users/authenticate', {
            method: 'Post',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify({
                'Email': this.refs.email.value,
                'Password': this.refs.password.value,
            })
        });

        const data = await response.json();

        console.log(data);

        this.handleRedirect(response, data);
    }

    handleRedirect(response, data) {
        if (response.status === 200) {
            sessionStorage.setItem('key', 'Bearer ' + data.token);
            window.location.href = '/';
        }
    }
}