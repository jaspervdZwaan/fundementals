﻿import React, { Component } from 'react';
import { Message } from './Message';

export class UserFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: []
        };
    }

    render() {
        let contents = this.state.messages.lengt !== 0 ? UserFeed.renderMessages(this.state.messages) : <a>It appears this user doesn't have any messages yet.</a>

        return (
            <div>
                {contents}
            </div>
        );
    }

    static renderMessages(messages) {
        return (
            messages.map(message => {
                return (
                    <Message message={message} key={message.id} />
                );
            })
        );
    }

    componentDidMount() {
        this.populateMessages();
    }

    async populateMessages() {
        const response = await fetch("api/v1.0/messages/user_feed?username=" + this.props.username);
        const data = await response.json();
        this.setState({ messages: data });
    }
}