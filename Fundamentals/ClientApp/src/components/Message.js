﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './css/message.css';

export class Message extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var message = this.props.message;

        return (
            <div className="card message">
                <div className="card-body">
                    <h3 className="card-title"><Link to={"/user/" + message.user.username}>{message.user.username}</Link></h3>
                    <p className="card-text">{message.text}</p>
                </div>
                <img className="card-img-bottom" src={message.image} />
            </div>
        );
    }
}
