﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './css/explore.css';
import { Message } from './Message';

export class Explore extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            messages: []
        };

        this.handeSubmit = this.handeSubmit.bind(this);
        this.populateUsers = this.populateUsers.bind(this);
        this.populateMessages = this.populateMessages.bind(this);
    }

    static renderUserResults(users) {
        return (
            <div>
                <div className='card users-header'>
                    <div className='card-body'>
                        <h3 className='card-title'>Users</h3>
                    </div>
                </div>
                {
                    users.map(user => {
                        return (
                            <div className='card user-result' key={user.id}>
                                <div className='card-body'>
                                    <h3 className='card-title'><a><Link to={'/user/' + user.username}>{user.username}</Link></a></h3>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );
    }

    static renderMessageResults(messages) {
        return (
            <div>
                <div className='card messages-header'>
                    <div className='card-body'>
                        <h3 className='card-title'>Messages</h3>
                    </div>
                </div>
                {
                    messages.map(message => {
                        return (
                            < Message message={message} />
                        );
                    })
                }
            </div>
        );
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handeSubmit} className='form-inline'>
                    <div className='form-group mx-sm-3 mb-2'>
                        <input type='text' className='form-control' ref='query' placeholder='Type something here' id='input'/>
                    </div>
                    <button type='submit' className='btn btn-primary mb-2'>Search</button>
                </form>
                <div>{Explore.renderUserResults(this.state.users)}</div>
                <div>{Explore.renderMessageResults(this.state.messages)}</div>
            </div>
        );
    }

    handeSubmit(event) {
        event.preventDefault();
        this.populateUsers();
        this.populateMessages();
    }

    async populateUsers() {
        const response = await fetch('api/v1.0/users/search?query=' + this.refs.query.value);
        const data = await response.json();
        this.setState({ users: data });
    }

    async populateMessages() {
        const response = await fetch('api/v1.0/messages/search?query=' + this.refs.query.value);
        const data = await response.json();
        this.setState({ messages: data });
    }
}