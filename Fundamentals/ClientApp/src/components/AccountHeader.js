﻿import React, { Component } from 'react';
import axios from 'axios';

export class AccountHeader extends Component {
    constructor(props) {
        super(props);

        this.populateUser = this.populateUser.bind(this);    
        this.followUser = this.followUser.bind(this);

        this.state = {
            user: {}
        }
    }

    render() {
        var user = this.state.user;

        return (
            <div className='card'>
                <div className='card-body'>
                    <h3 className='card-title'>{this.state.user.username}</h3>
                    <p className='card-text'>{this.state.user.description}</p>
                </div>
                <img className='card-img-bottom' src={this.state.user.image}></img>
                <a class="btn btn-primary" onClick={this.followUser} style={{color: '#ffffff'}}>Follow</a>
            </div>
        );
    }

    componentDidMount() {
        this.populateUser();
    }

     populateUser() {
        const response = axios.get('api/v1.0/users/get?username=' + this.props.username).then(response => this.setState({user: response.data})).catch(err => console.log(err));
        console.log(response.data);
        //const response = await fetch('api/v1.0/users/get?username=' + this.props.username);
        //const data = await response.json();
        //this.setState({ user: data });
    }

    async followUser() {
        const response = await fetch('api/v1.0/friendships', {
            method: "Post",
            headers: {
                "Content-Type": "application/json",
                Authorization: sessionStorage.getItem('key')
            },
            body: JSON.stringify({
                "FriendId": this.state.user.id
            })
        });
    }

    handleFollowResult() {

    }
}