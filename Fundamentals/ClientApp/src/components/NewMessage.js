﻿import React, { Component } from 'react';
import axios from 'axios';

export class NewMessage extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleResult = this.handleResult.bind(this);

        this.state = {
            alert: ""
        };
    }

    render() {
        let alert = this.state.alert === "succes" ? NewMessage.succesMessage() : this.state.alert === "failed" ? NewMessage.errorMessage() : <a></a>
        console.log(alert);

        return (
            <form onSubmit={this.handleSubmit} encType="multipart/form-data" id="form">
                <div>{alert}</div>
                <div className="form-group">
                    <label htmlFor="text-input">Text</label>
                    <input type='text'  id="text-input" className="form-control" name="Text" placeholder="Text" ref='text'/>
                </div>
                <div className="custom-file">
                    <input type='file'  id='file-selector' className="custom-file-input" name="Image"/>
                    <label className="custom-file-label" htmlFor="file-selector">Choose an image</label>
                </div>
                <div>
                    <button type='submit' className="btn btn-primary" name="Button" style={{ marginTop: 12 }} onClick={this.handleSubmit}>Post</button>
                </div>
            </form>
        );
    }

    async handleSubmit(event) {
        event.preventDefault();

       await axios.post('api/v1.0/messages', new FormData(document.getElementById('form')), {
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': sessionStorage.getItem('key')
            }
        }).then((response) => this.handleResult(response)).catch((err) => console.log(err));
    }

    handleResult(response) {
        console.log(response);
        if (response.status === 201) {
            this.setState({ status: "succes" });
        } else {
            this.setState({ status: "failed" });
        }
    }

    static errorMessage() {
        return (
            <div class="alert alert-danger" role="alert">
                Looks like something went wrong.
            </div>
        );
    }

    static succesMessage() {
        return (
            <div class="alert alert-success" role="alert" id="succes">
                Message posted succesfully.
            </div>
        );
    }
}
