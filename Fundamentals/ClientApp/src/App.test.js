import '@testing-library/jest-dom';
import axios from 'axios';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import TestRenderer from 'react-test-renderer';
import App from './App';
import { Message } from './components/Message';
import { NewMessage } from './components/NewMessage';
import { User } from './components/User';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('axios');

describe('NewMessage Component', () => {
    describe('Post message when clicked', () => {
        it('A post request should be made.', () => {
            const message = {
                user: {
                    username: "Test"
                },
                text: "This is a test"
            };

            const wrapper = mount(<NewMessage></NewMessage>);

            axios.post.mockImplementation(() => Promise.resolve({ status: 201, data: message }));

            const postSpy = jest.spyOn(axios, 'post');

            const input = wrapper.find('#text-input');

            input.simulate('change', { target: { value: 'This is a test' } });

            const button = wrapper.find('button');

            button.simulate('click');

            expect(postSpy).toBeCalled();

            expect(wrapper.state()).toContain('succes');
        });
    });
});

it('renders without crashing', async () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <MemoryRouter>
            <App />
        </MemoryRouter>, div);
    await new Promise(resolve => setTimeout(resolve, 1000));
});

test("Render message.", () => {
    const message = {
        user: {
            id: 1,
            username: "test"
        },
        location: {
            latitude: 1,
            longitude: 1
        },
        text: "This is a test."
    };

    const tree = TestRenderer.create(
        <MemoryRouter>
            <Message message={message} />
        </MemoryRouter>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});

test("Render user", () => {
    const user = {
        username: "Test",
        description: "This is a test.",
        image: 'http://tinyurl.com/yatzpcu4'
    };

    const tree = TestRenderer.create(
        <MemoryRouter>
            <User user={user} />
        </MemoryRouter>
    ).toJSON();

    expect(tree).toMatchSnapshot();
});
