﻿using Models.Relations;

namespace Responses.Relations
{
    public class FriendshipResponse : ResponseBase<Friendship>
    {
        public FriendshipResponse(Friendship friendship) : base(friendship) { }

        public FriendshipResponse(string errorMessage) : base(errorMessage) { }
    }
}
