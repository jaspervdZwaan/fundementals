﻿namespace Responses
{
    public abstract class ResponseBase<TEntity>
    {
        public bool CompletedSuccesfully { get; set; }

        public string ErrorMessage { get; set; }

        public TEntity Entity { get; set; }

        protected ResponseBase(TEntity entity)
        {
            CompletedSuccesfully = true;
            ErrorMessage = string.Empty;
            Entity = entity;
        }

        protected ResponseBase(string errorMessage)
        {
            CompletedSuccesfully = false;
            ErrorMessage = errorMessage;
            Entity = default;
        }
    }
}
