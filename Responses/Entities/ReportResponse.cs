﻿using Models.Entities;

namespace Responses.Entities
{
    public class ReportResponse : ResponseBase<Report>
    {
        public ReportResponse(Report report) : base(report)
        {

        }

        public ReportResponse(string errorMessage) : base(errorMessage)
        {

        }
    }
}
