﻿using Models.Entities;

namespace Responses.Entities
{
    public class UserResponse : ResponseBase<User>
    {
        public UserResponse(User user) : base(user)
        {

        }

        public UserResponse(string errorMessage) : base(errorMessage)
        {

        }
    }
}
