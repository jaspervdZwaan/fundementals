﻿using Models.Entities;

namespace Responses.Entities
{
    public class MessageResponse : ResponseBase<Message>
    {
        public MessageResponse(Message message) : base(message)
        {

        }

        public MessageResponse(string errorMessage) : base(errorMessage)
        {

        }
    }
}
