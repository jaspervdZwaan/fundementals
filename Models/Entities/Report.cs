﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.Entities
{
    public class Report
    {
        [Key]
        public int Id { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

        public Message Message { get; set; }

        public int MessageId { get; set; }

#nullable enable

        public string? Description { get; set; }

        public string? Category { get; set; }

#nullable disable

        public DateTime CreatedAt { get; set; }
    }
}
