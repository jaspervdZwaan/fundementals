﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Entities
{
    public class Message
    {
        [Key]
        public int Id { get; set; }

        public User User { get; set; }

        public int UserId { get; set; }

#nullable enable

        public string? Text { get; set; }

        public string? Image { get; set; }

        public Location? Location { get; set; }

#nullable disable

        public DateTime CreatedAt { get; set; }

        public List<Report> Reports { get; set; }
    }
}
