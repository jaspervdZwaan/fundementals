﻿using Models.Relations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

#nullable enable

        public string? Description { get; set; }

        public string? Location { get; set; }

        public string? Image { get; set; }

#nullable disable

        public DateTime CreatedAt { get; set; }

        public List<Message> Messages { get; set; }

        public List<Report> Reports { get; set; }
    }
}
