﻿using Models.Entities;
using System.ComponentModel.DataAnnotations;

namespace Models.Relations
{
    public class Friendship
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int FriendId { get; set; }

        public User Friend { get; set; }
    }
}
