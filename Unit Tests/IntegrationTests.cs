using Fundamentals;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Tests
{
    public class IntegrationTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> Factory;

        public IntegrationTests(WebApplicationFactory<Startup> factory)
        {
            Factory = factory;
        }

        [Theory]
        [InlineData("/api/v1.0/users")]
        [InlineData("/api/v1.0/users/search?query=test")]
        [InlineData("/api/v1.0/messages")]
        [InlineData("/api/v1.0/messages/search?query=test")]
        [InlineData("/api/v1.0/messages/user_feed")]
        public async Task Get(string url)
        {
            using HttpClient client = Factory.CreateClient();

            using HttpResponseMessage response = await client.GetAsync(url);

            _ = response.EnsureSuccessStatusCode();

            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
