﻿using Azure.Storage.Blobs;
using Interfaces.Services.Blob;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Services
{
    public class BlobService : IBlobService
    {
        public async Task<string> UploadAsync(IFormFile image)
        {
            BlobContainerClient container = new BlobContainerClient("UseDevelopmentStorage=true", "fundementals");

            _ = await container.CreateIfNotExistsAsync();

            BlobClient blobClient = container.GetBlobClient(Guid.NewGuid().ToString() + Path.GetExtension(image.FileName));

            _ = await blobClient.UploadAsync(image.OpenReadStream());

            return blobClient.Uri.AbsoluteUri;
        }
    }
}
