﻿using Data.Contexts;
using Interfaces.Services.Authentication;
using Interfaces.Services.Entities;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using Responses.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Entities
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly IAuthenticationService AuthenticationSerivce;

        public UserService(FundementalsDbContext context, IAuthenticationService authenticationService) : base(context)
        {
            AuthenticationSerivce = authenticationService;
        }

        public async Task<string> AuthenticateAsync(User user)
        {
            return AuthenticationSerivce.GetToken(await Context.Users.FirstOrDefaultAsync(entity => entity.Email == user.Email & entity.Password == user.Password));
        }

        public async Task<UserResponse> CreateAsync(User user)
        {
            try
            {
                var result = await Context.Users.AddAsync(user);
                _ = await Context.SaveChangesAsync();
                return new UserResponse(result.Entity);
            }
            catch (DbUpdateException exception)
            {
                return new UserResponse(exception.Message);
            }
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await Context.Users.AsNoTracking().ToListAsync();
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await Context.Users.FindAsync(id);
        }

        public async Task<UserResponse> RemoveAsync(int id)
        {
            try
            {
                User existingUser = await Context.Users.FindAsync(id);
                _ = Context.Users.Remove(existingUser);
                _ = await Context.SaveChangesAsync();
                return new UserResponse(existingUser);
            }
            catch (DbUpdateException exception)
            {
                return new UserResponse(exception.Message);
            }
        }

        public async Task<UserResponse> UpdateAsync(User user, int id)
        {
            User existingUser = await Context.Users.FindAsync(id);

            existingUser.Username = user.Username;
            existingUser.Email = user.Email;
            existingUser.Description = user.Description;
            existingUser.Location = user.Location;

            try
            {
                _ = Context.Users.Update(existingUser);
                _ = await Context.SaveChangesAsync();
                return new UserResponse(existingUser);
            }
            catch (DbUpdateException exception)
            {
                return new UserResponse(exception.Message);
            }
        }

        public async Task<User> GetByUsername(string username)
        {
            User existingUser = await Context.Users.FirstOrDefaultAsync(user => user.Username == username);

            if (existingUser == null)
            {
                return new User();
            }

            return await GetByIdAsync(existingUser.Id);
        }

        public async Task<IEnumerable<User>> Search(string query)
        {
            return await Context.Users.Where(user => user.Username.Contains(query)).ToListAsync();
        }
    }
}
