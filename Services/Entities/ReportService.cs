﻿using Data.Contexts;
using Interfaces.Services.Entities;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using Responses.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Entities
{
    public class ReportService : ServiceBase, IReportService
    {
        public ReportService(FundementalsDbContext context) : base(context)
        {

        }

        public async Task<ReportResponse> CreateAsync(Report report)
        {
            try
            {
                var result = await Context.Reports.AddAsync(report);
                _ = await Context.SaveChangesAsync();
                return new ReportResponse(result.Entity);
            }
            catch (Exception exception)
            {
                return new ReportResponse(exception.Message);
            }
        }

        public async Task<IEnumerable<Report>> GetAllAsync()
        {
            return await Context.Reports.AsNoTracking().ToListAsync();
        }

        public async Task<Report> GetByIdAsync(int id)
        {
            return await Context.Reports.FindAsync(id);
        }

        public async Task<ReportResponse> RemoveAsync(int id)
        {
            Report existingReport = await Context.Reports.FindAsync(id);

            try
            {
                _ = Context.Reports.Remove(existingReport);
                _ = await Context.SaveChangesAsync();
                return new ReportResponse(existingReport);
            }
            catch (DbUpdateException exception)
            {
                return new ReportResponse(exception.Message);
            }
        }

        public async Task<ReportResponse> UpdateAsync(Report report, int id)
        {
            Report existingReport = await Context.Reports.FindAsync(id);

            report.Description = report.Description;
            report.Category = report.Category;

            try
            {
                _ = Context.Reports.Update(existingReport);
                _ = await Context.SaveChangesAsync();
                return new ReportResponse(existingReport);
            }
            catch (DbUpdateException exception)
            {
                return new ReportResponse(exception.Message);
            }
        }
    }
}
