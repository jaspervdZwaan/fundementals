﻿using Data.Contexts;
using Interfaces.Services.Entities;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using Models.Relations;
using Responses.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Entities
{
    public class MessageService : ServiceBase, IMessageService
    {
        public MessageService(FundementalsDbContext context) : base(context)
        {

        }

        public async Task<MessageResponse> CreateAsync(Message message)
        {
            message.CreatedAt = DateTime.Now;

            try
            {
                var result = await Context.Messages.AddAsync(message);
                _ = await Context.SaveChangesAsync();
                return new MessageResponse(result.Entity);
            }
            catch (DbUpdateException exception)
            {
                return new MessageResponse(exception.Message);
            }
        }

        public async Task<IEnumerable<Message>> GetAllAsync()
        {
            return await Context.Messages.AsNoTracking().ToListAsync();
        }

        public async Task<Message> GetByIdAsync(int id)
        {
            return await Context.Messages.FindAsync(id);
        }

        public async Task<MessageResponse> RemoveAsync(int id)
        {
            Message existingMessage = await Context.Messages.FindAsync(id);

            try
            {
                _ = Context.Messages.Remove(existingMessage);
                _ = await Context.SaveChangesAsync();
                return new MessageResponse(existingMessage);
            }
            catch (DbUpdateException exception)
            {
                return new MessageResponse(exception.Message);
            }
        }

        public async Task<MessageResponse> UpdateAsync(Message message, int id)
        {
            Message existingMessage = await Context.Messages.FindAsync(id);

            existingMessage.Text = message.Text;
            existingMessage.Image = message.Image;
            existingMessage.Location = message.Location; 

            try
            {
                _ = Context.Messages.Update(existingMessage);
                _ = await Context.SaveChangesAsync();
                return new MessageResponse(existingMessage);
            }
            catch (DbUpdateException exception)
            {
                return new MessageResponse(exception.Message);
            }
        }

        public async Task<IEnumerable<Message>> GetUserFeed(string username)
        {
            User user = await Context.Users.FirstOrDefaultAsync(user => user.Username == username);

            if (user == null)
            {
                return new List<Message>();
            }

            return await GetUserFeed(user.Id);
        }

        public async Task<IEnumerable<Message>> GetUserFeed(int userId)
        {
            return await Context.Messages.Where(message => message.UserId == userId).ToListAsync();
        }

        public async Task<IEnumerable<Message>> Search(string query)
        {
            return await Context.Messages.Where(message => message.Text.Contains(query)).Include(message => message.User).ToListAsync();
        }

        public async Task<IEnumerable<Message>> GetHomeFeed(int userId)
        {
            List<int> friendIds = await Context.Friendships.Where(dbFriendship => dbFriendship.UserId == userId).Select(dbFriendship => dbFriendship.FriendId).ToListAsync();

            List<Message> messages = new List<Message>();

            foreach(int friendId in friendIds)
            {
                messages.AddRange(await Context.Messages.Where(dbMessage => dbMessage.UserId == friendId).Include(dbMessage => dbMessage.User).ToListAsync());
            }

            return messages;
        }
    }
}
