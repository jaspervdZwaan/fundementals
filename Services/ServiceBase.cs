﻿using Data.Contexts;

namespace Services
{
    public class ServiceBase
    {
        protected readonly FundementalsDbContext Context;

        public ServiceBase(FundementalsDbContext context)
        {
            Context = context;
        }
    }
}
