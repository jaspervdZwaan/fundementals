﻿using Data.Contexts;
using Interfaces.Services.Relations;
using Microsoft.EntityFrameworkCore;
using Models.Relations;
using Responses.Relations;
using Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FestiFind.Logic.Services
{
    public class FriendshipService : ServiceBase, IFriendshipService
    {
        public FriendshipService(FundementalsDbContext context) : base(context) { }

        public async Task<FriendshipResponse> CreateAsync(Friendship friendship)
        {
            Friendship existingFriendship = await Context.Friendships.FirstOrDefaultAsync(dbFriendship => dbFriendship.UserId == friendship.UserId & dbFriendship.FriendId == friendship.FriendId);

            if (existingFriendship != null)
            {
                return new FriendshipResponse("Friendship already exists.");
            }

            try
            {
                _ = await Context.Friendships.AddAsync(friendship);
                _ = await Context.SaveChangesAsync();
                return new FriendshipResponse(friendship);
            }
            catch (DbUpdateException exception)
            {
                return new FriendshipResponse(exception.InnerException.Message);
            }
        }

        public async Task<IEnumerable<Friendship>> GetAllAsync()
        {
            return await Context.Friendships.AsNoTracking().ToListAsync();
        }

        public async Task<FriendshipResponse> RemoveAsync(Friendship friendship)
        {
            Friendship existingFriendship = await Context.Friendships.FirstOrDefaultAsync(dbFriendship => dbFriendship.UserId == friendship.UserId & dbFriendship.FriendId == friendship.FriendId);

            if( existingFriendship == null)
            {
                return new FriendshipResponse("Relation not found.");
            }

            try
            {
                _ = Context.Remove(existingFriendship);
                _ = await Context.SaveChangesAsync();
                return new FriendshipResponse(friendship);
            }
            catch (DbUpdateException exception)
            {
                return new FriendshipResponse(exception.InnerException.Message);
            }
        }

        public async Task<FriendshipResponse> UpdateAsync(Friendship friendship)
        {
            Friendship existingFriendship = await Context.Friendships.FirstOrDefaultAsync(dbFriendship => dbFriendship.UserId == friendship.UserId & dbFriendship.FriendId == friendship.FriendId);

            try
            {
                existingFriendship = friendship;
                _ = Context.Update(existingFriendship);
                _ = await Context.SaveChangesAsync();
                return new FriendshipResponse(existingFriendship);
            }
            catch (DbUpdateException exception)
            {
                return new FriendshipResponse(exception.InnerException.Message);
            }
        }
    }
}
