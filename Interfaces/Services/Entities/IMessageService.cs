﻿using Models.Entities;
using Responses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces.Services.Entities
{
    public interface IMessageService
    {
        Task<IEnumerable<Message>> GetAllAsync();
        Task<Message> GetByIdAsync(int id);
        Task<MessageResponse> CreateAsync(Message message);
        Task<MessageResponse> UpdateAsync(Message message, int id);
        Task<MessageResponse> RemoveAsync(int id);
        Task<IEnumerable<Message>> GetUserFeed(string username);
        Task<IEnumerable<Message>> GetUserFeed(int userId);
        Task<IEnumerable<Message>> Search(string query);
        Task<IEnumerable<Message>> GetHomeFeed(int userId);
    }
}
