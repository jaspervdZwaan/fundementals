﻿using Models.Entities;
using Responses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces.Services.Entities
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAllAsync();
        Task<IEnumerable<User>> Search(string query);
        Task<User> GetByIdAsync(int id);
        Task<User> GetByUsername(string username);
        Task<UserResponse> CreateAsync(User user);
        Task<UserResponse> UpdateAsync(User user, int id);
        Task<UserResponse> RemoveAsync(int id);
        Task<string> AuthenticateAsync(User user);
    }
}
