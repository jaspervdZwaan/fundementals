﻿using Models.Entities;
using Responses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces.Services.Entities
{
    public interface IReportService
    {
        Task<IEnumerable<Report>> GetAllAsync();
        Task<Report> GetByIdAsync(int id);
        Task<ReportResponse> CreateAsync(Report report);
        Task<ReportResponse> UpdateAsync(Report report, int id);
        Task<ReportResponse> RemoveAsync(int id);
    }
}
