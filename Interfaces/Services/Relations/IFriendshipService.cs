﻿using Models.Relations;
using Responses.Relations;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Interfaces.Services.Relations
{
    public interface IFriendshipService
    {
        Task<IEnumerable<Friendship>> GetAllAsync();
        Task<FriendshipResponse> CreateAsync(Friendship friendship);
        Task<FriendshipResponse> RemoveAsync(Friendship friendship);
        Task<FriendshipResponse> UpdateAsync(Friendship friendship);
    }
}
