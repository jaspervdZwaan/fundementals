﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces.Services.Blob
{
    public interface IBlobService
    {
        Task<string> UploadAsync(IFormFile image);
    }
}
