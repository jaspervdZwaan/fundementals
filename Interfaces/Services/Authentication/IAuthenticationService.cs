﻿using Models.Entities;

namespace Interfaces.Services.Authentication
{
    public interface IAuthenticationService
    {
        string GetToken(User user);
    }
}
