FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Fundamentals/Fundamentals.csproj", "Fundamentals/"]
COPY ["Interfaces/Interfaces.csproj", "Interfaces/"]
COPY ["Models/Models.csproj", "Models/"]
COPY ["Responses/Responses.csproj", "Responses/"]
COPY ["Data/Data.csproj", "Data/"]
COPY ["Resources/Resources.csproj", "Resources/"]
COPY ["Services/Services.csproj", "Services/"]
RUN dotnet restore "Fundamentals/Fundamentals.csproj"
COPY . .
WORKDIR "/src/Fundamentals"
RUN dotnet build "Fundamentals.csproj" -c Release -o /app/build



FROM base AS final
WORKDIR /app
COPY --from=build /app/build .
ENTRYPOINT ["dotnet", "Fundamentals.dll"]
